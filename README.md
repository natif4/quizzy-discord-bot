# Quizzy - Discord Bot

Quizzy est un bot Discord en développement. Il a pour but de faire un jeux de quiz pour les utilisateurs d'un ou plusieurs serveurs Discord. Le bot porte le nom de Quizzy et il est programmé en JavaScript. 

## Prérequis

### Node.js

Le programme est exécuté à l'aide de Node.Js. Pour l'installer, il faut choisir la version la plus récente sur la page suivante : https://nodejs.org/en/

### Créer un bot

Afin de pouvoir utiliser le programme, vous devez créé votre propre entité de bot Discord. Ce tutoriel vous montrera exactement comment faire ces étapes : https://www.ionos.com/digitalguide/server/know-how/creating-discord-bot/

### Fichier de configuration

Afin de pouvoir exécuter le programme, il faut créé un fichier nommé `config.json` à la racine et y ajouté le contenu suivant : 

```json
{
	"token": "LeTokenDeVotreBot",
	"clientId": "LeClientIdDeVotreBot",
	"guildId": "LeGuildIdDeVotreServeurTest"
}
```

Il est possible d'obtenir le `clientId` et le `guildId` en activant le mode développeur dans les paramètres Discord et en faisant clique droit sur l'icône du serveur ou du bot et sélectionner l'option "Copier l'identifiant". 

### Installation des dépendances

Dans un terminal à la racine du projet, il faut exécuter la commande suivante : 
```shell
npm install
```

Cela installera les dépendances requises pour le programme. 

## Fonctionnement du programme

### Déploiement des commandes

Pour le moment, les fonctionnalités du bot sont fonctionnelles que pour le serveur spécifié dans le fichier `config.json`. Afin de déployer les commandes du bot au serveur, il faut exécuter le fichier `deploy-commands.js` à l'aide de commande suivante dans un terminal : 

```shell
node deploy-commands.js
```

Cette commande doit être exécuté à chaque fois qu'un fichier de commandes est modifié. Sinon, la modification ne s'appliquera pas dans le serveur.

 ### Démarrer le programme 

Pour démarrer le programme, il faut exécuté la commande suivante dans un terminal : 

```shell
node index.js
```

## Standard de programmation

Le programme utilise les standards de programmation JavaScript de Google. Pour consulter les règles du standard, veuillez cliquer sur le lien suivant : https://google.github.io/styleguide/jsguide.html

## Serveur discord

Vous pouvez rejoindre le serveur Discord pour testé le jeu et parler du projet à l'aide du lien suivant : https://discord.gg/kMe77CTKSB

## Licence du projet

MIT License

Copyright (c) 2023 Gabriel Babin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

