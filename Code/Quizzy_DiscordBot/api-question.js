const {quiz} = require('./quiz');

/**
 * Get the questions from the Trivia API.
 *
 */
function getQuestions() {
  fetch('https://the-trivia-api.com/api/questions?limit=' + quiz.questionLimit +
    '&region=' + quiz.region + '&difficulty=' + quiz.difficulty, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
    },
  }).then((response) => response.text())
      .then((data) => quiz.questions = JSON.parse(data));
}

module.exports = {getQuestions};
