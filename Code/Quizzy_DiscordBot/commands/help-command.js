const {SlashCommandBuilder} = require('discord.js');
const {returnEmbebHelp, returnEmbedHelpSet} = require('../embeds.js');

module.exports = {
  data: new SlashCommandBuilder()
      .setName('help')
      .setDescription('Explain all the commands of the bot')
      .addSubcommand((subcommand) =>
        subcommand
            .setName('game')
            .setDescription('Describe the game\'s commands.'))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('set')
            .setDescription('Describe the "set" command.')),
  async execute(interaction) {
    if (interaction.options.getSubcommand() === 'set') {
      interaction.reply({embeds: [(returnEmbedHelpSet())]});
    } else if (interaction.options.getSubcommand() === 'game') {
      interaction.reply({embeds: [returnEmbebHelp()]});
    }
  },
};
