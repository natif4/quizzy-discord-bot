const {SlashCommandBuilder} = require('discord.js');
const {quiz} = require('../quiz');

module.exports = {
  data: new SlashCommandBuilder()
      .setName('set')
      .setDescription('Set an element of the game. (Region, question\'s ' +
        'limit and difficulty')
      .addSubcommand((subcommand) =>
        subcommand
            .setName('difficulty')
            .setDescription('Set the difficulty of the game')
            .addStringOption((option) =>
              option.setName('difficulty')
                  .setDescription(('The difficulty of the game'))
                  .setChoices(
                      {name: 'easy', value: 'easy'},
                      {name: 'medium', value: 'medium'},
                      {name: 'hard', value: 'hard'},
                  )))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('region')
            .setDescription('Set the region of the game')
            .addStringOption((option) =>
              option.setName('region')
                  .setDescription('The region of the game')
                  .setChoices(
                      {name: 'Argentina', value: 'AR'},
                      {name: 'Australia', value: 'AU'},
                      {name: 'Austria', value: 'AT'},
                      {name: 'Belgium', value: 'BE'},
                      {name: 'Brazil', value: 'BR'},
                      {name: 'Canada', value: 'CA'},
                      {name: 'China', value: 'CN'},
                      {name: 'Denmark', value: 'DK'},
                      {name: 'Finland', value: 'FI'},
                      {name: 'France', value: 'FR'},
                      {name: 'Germany', value: 'DE'},
                      {name: 'Italy', value: 'IT'},
                      {name: 'Japan', value: 'JP'},
                      {name: 'Mexico', value: 'MX'},
                      {name: 'Netherlands', value: 'NL'},
                      {name: 'Poland', value: 'PL'},
                      {name: 'Portugal', value: 'PT'},
                      {name: 'Russia', value: 'RU'},
                      {name: 'Saudi Arabia', value: 'SA'},
                      {name: 'Spain', value: 'ES'},
                      {name: 'Sweden', value: 'SE'},
                      {name: 'Switzerland', value: 'CH'},
                      {name: 'Ukraine', value: 'UA'},
                      {name: 'United States of America', value: 'US'},
                  )))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('limit')
            .setDescription('Set the limit of questions of the game')
            .addIntegerOption((option) =>
              option.setName('limit')
                  .setDescription('The limit of questions of the game'))),
  async execute(interaction) {
    if (quiz.gameInitialized) {
      if (!quiz.gameStarted) {
        if (interaction.user.tag === quiz.gameLeader) {
          if (interaction.options.getSubcommand() === 'difficulty') {
            setDifficulty(interaction);
          } else if (interaction.options.getSubcommand() === 'region') {
            setRegion(interaction);
          } else if (interaction.options.getSubcommand() === 'limit') {
            setLimit(interaction);
          }
        } else {
          interaction.reply('Only the game leader can change the parameters ' +
            'of the game.');
        }
      } else {
        interaction.reply('You cannot change the parameters of the game ' +
          'because it\'s already been start.');
      }
    } else {
      interaction.reply('You have to create a game with "/quiz new" to set ' +
        'the parameters');
    }
  },
};

/**
 * Set the difficulty of the game.
 *
 * @param {object} interaction The interaction of the command.
 */
function setDifficulty(interaction) {
  const difficulty = interaction.options.getString('difficulty');
  if (difficulty !== null) {
    quiz.difficulty = difficulty;
    interaction.reply('The difficulty is now set to ' + difficulty);
  } else {
    interaction.reply('You have to specified the difficulty you want');
  }
}

/**
 * Set the limit of questions of the game.
 *
 * @param {object} interaction The interaction of the command.
 */
function setLimit(interaction) {
  const limit = interaction.options.getInteger('limit');
  if (limit <= 20) {
    quiz.questionLimit = limit;
    interaction.reply('The limit of questions is now set to ' + limit);
  } else if (limit > 20) {
    interaction.reply('The limit can\'t be higher than 20.');
  } else {
    interaction.reply('This option isn\'t valid.');
  }
}

/**
 * Set the region of the game.
 *
 * @param {object} interaction The interaction of the command.
 */
function setRegion(interaction) {
  const region = interaction.options.getString('region');
  if (region !== null) {
    quiz.region = region;
    interaction.reply('The region is now set to ' + region);
  } else {
    interaction.reply('You have to specified the region you want');
  }
}

