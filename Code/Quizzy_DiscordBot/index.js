const {Client, Collection, GatewayIntentBits} = require('discord.js');
const fs = require('node:fs');
const path = require('node:path');
const {token} = require('./config.json');

// Create the client
const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.GuildMembers,
  ],
});

// Add commands to the client
client.commands = new Collection();
const pathCommands = path.join(__dirname, 'commands');
const commandsFiles =
  fs.readdirSync(pathCommands).filter((file) => file.endsWith('.js'));
for (const file of commandsFiles) {
  const pathFile = path.join(pathCommands, file);
  const command = require(pathFile);
  if ('data' in command && 'execute' in command) {
    client.commands.set(command.data.name, command);
  } else {
    console.log('[WARNING] The command at ' + pathFile + ' is missing a ' +
        'required "data" or "execute" property.');
  }
}

// Add events to the client
const pathEvents = path.join(__dirname, 'events');
const eventsFile =
  fs.readdirSync(pathEvents).filter((file) => file.endsWith('.js'));

for (const file of eventsFile) {
  const pathFile = path.join(pathEvents, file);
  const event = require(pathFile);
  if (event.once) {
    client.once(event.name, (...args) => event.execute(...args));
  } else {
    client.on(event.name, (...args) => event.execute(...args));
  }
}

// Connect the client to Discord
client.login(token);
