/**
 * Quiz is the game that the bot offer.
 *
 */
class Quiz {
  /**
   * Constructor
   *
   * @param {boolean} gameInitialized Determine if the game is active or not.
   * @param {boolean} gameStarted Determine if the game is currently playing.
   * @param {number} playersNum Number of players in the game
   * @param {Array} playersName All the name's players
   * @param {number} maxPlayers The maximum number of players in the game
   * @param {string} gameLeader The game leader of the game
   * @param {string} region The region of the game
   * @param {string} difficulty The difficulty of the game
   * @param {Array} questions The questions of the game
   * @param {number} questionLimit The limit of questions of the game
   * @param {number} counter Determine which questions the game's at
   * @param {boolean} questionMode Determine if the game is in question mode
   *  or not.
   * @param {Array} playersID All the ID's players
   * @param {Array} playersPoints Points of each player.
   */
  constructor(gameInitialized, gameStarted, playersNum, playersName, maxPlayers,
      gameLeader, region, difficulty, questions, questionLimit, counter,
      questionMode, playersID, playersPoints) {
    this.gameInitialized = gameInitialized;
    this.gameStarted = gameStarted;
    this.playersNum = playersNum;
    this.playersName = playersName;
    this.maxPlayers = maxPlayers;
    this.gameLeader = gameLeader;
    this.region = region;
    this.difficulty = difficulty;
    this.questions = questions;
    this.questionLimit = questionLimit;
    this.counter = counter;
    this.questionMode = questionMode;
    this.playersID = playersID;
    this.playersPoints = playersPoints;
  }
}

quiz = new Quiz(false, false, 0, [], 4, '', 'CA', 'easy', [], 10, 0, false, [],
    []);

module.exports = {quiz};
