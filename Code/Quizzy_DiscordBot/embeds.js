const {EmbedBuilder, Colors} = require('discord.js');

/**
 * Return an embed who shows the players of the game.
 *
 * @param {Array} players Array of player's name.
 * @param {string} difficulty The difficulty of the game
 * @param {string} region The region of the game
 * @param {string} limitQuestions The limit of question of the game
 * @return {EmbedBuilder} Embed
 */
function returnEmbebPlayers(players, difficulty, region, limitQuestions) {
  return new EmbedBuilder()
      .setColor(Colors.Yellow)
      .setTitle('Quiz game ❔')
      .setDescription(`Players of the game :\n\n` +
        `Player 1 (Game leader) : ${players[0] !== undefined ?
          players[0] : ''}\n` +
        `Player 2 : ${players[1] !== undefined ? players[1] : ''}\n` +
        `Player 3 : ${players[2] !== undefined ? players[2] : ''}\n` +
        `Player 4 : ${players[3] !== undefined ? players[3] : ''}\n`)
      .addFields(
          {
            name: 'Difficulty',
            value: difficulty,
            inline: true,
          },
          {
            name: 'Region',
            value: region,
            inline: true,
          },
          {
            name: 'Limit of questions',
            value: limitQuestions,
            inline: true,
          },
      );
}

/**
 * Return an embed who shows the leaderboard of the game
 *
 * @param {Array} players The players of the games
 * @param {Array} playersPoints Points of every player of the game
 * @return {EmbedBuilder} Embed
 */
function returnEmbedLeaderboard(players, playersPoints) {
  return new EmbedBuilder()
      .setColor(Colors.Fuchsia)
      .setTitle('Leaderboard 🏆')
      .setDescription(returnLeaderboardText(players, playersPoints));
}

/**
 * Return an embed who shows the help menu.
 *
 * @return {EmbedBuilder} Embed
 */
function returnEmbebHelp() {
  return new EmbedBuilder()
      .setColor(Colors.Purple)
      .setTitle('Help menu ℹ️')
      .setDescription('Welcome in the help menu of Quizzy Bot. Quizzy offer' +
        ' a quiz game for all users of the guild. Here are the different' +
        ' commands of Quizzy')
      .addFields(
          {
            name: '/help game ℹ️',
            value: 'You know what it does now !',
            inline: false,
          },
          {
            name: '/quiz ❔',
            value: 'Open a quiz game and/or show some infos',
            inline: false,
          },
          {
            name: '/start ▶️',
            value: 'Start the actual game',
            inline: false,
          },
          {
            name: '/join ✅',
            value: 'Join the actual game',
            inline: false,
          },
          {
            name: '/quit ❎',
            value: 'Quit the actual game',
            inline: false,
          },
          {
            name: '/reg 🏳️',
            value: 'Show the actual region of the game and all the regions ' +
              'that can be used',
            inline: false,
          },
          {
            name: '/set ⚙️',
            value: 'Write "/help set" to know more',
            inline: false,
          },
      );
}

/**
 * Return an embed that explain the set command.
 *
 * @return {EmbedBuilder} Embed
 */
function returnEmbedHelpSet() {
  return new EmbedBuilder()
      .setColor(Colors.Blue)
      .setTitle('Help menu - Command Set ℹ️ ⚙️')
      .setDescription('The command set has many options. You can choose to' +
        ' set the difficulty, the limit of questions and the region of the' +
        ' game. You have to write it this way : "/set ("region", "difficulty"' +
        ' or "limit") \'value you want to put here\'."')
      .addFields(
          {
            name: 'limit',
            value: 'You can put a number from 1 to 20 only',
            inline: false,
          },
          {
            name: 'difficulty',
            value: 'Possible choices : "easy", "medium",' +
              ' "hard"',
            inline: false,
          },
          {
            name: 'region',
            value: 'Write "/reg" to see what choices you can put',
            inline: false,
          },
      );
}

/**
 * Return an embed who indicates that the game is over.
 *
 * @param {string} description Text that explain why the game is over
 * @return {EmbedBuilder} Embed
 */
function returnEmbedGameEnded(description) {
  return new EmbedBuilder()
      .setColor(Colors.Default)
      .setTitle('Game ended 🚨')
      .setDescription(description);
}

/**
 * Return an embed that shows the actual region and all regions available.
 *
 * @param {string} region Actual region of the game
 * @return {EmbedBuilder} Embed
 */
function returnEmbedShowRegion(region) {
  return new EmbedBuilder()
      .setColor(Colors.Green)
      .setTitle('Region 🏳️')
      .setDescription('The questions are based on the region. If you change ' +
        'region, the questions will be related with the specified country. ' +
        'The actual region of the game is : ' + region + '\n\n' +
        'Here are all the available regions :')
      .addFields(
          {name: 'Argentina 🇦🇷', value: 'AR', inline: true},
          {name: 'Australia 🇦🇺', value: 'AU', inline: true},
          {name: 'Austria 🇦🇹', value: 'AT', inline: true},
          {name: 'Belgium 🇧🇪', value: 'BE', inline: true},
          {name: 'Brazil 🇧🇷', value: 'BR', inline: true},
          {name: 'Canada 🇨🇦', value: 'CA', inline: true},
          {name: 'China 🇨🇳', value: 'CN', inline: true},
          {name: 'Denmark 🇩🇰', value: 'DK', inline: true},
          {name: 'Finland 🇫🇮', value: 'FI', inline: true},
          {name: 'France 🇫🇷', value: 'FR', inline: true},
          {name: 'Germany 🇩🇪', value: 'DE', inline: true},
          {name: 'Italy 🇮🇹', value: 'IT', inline: true},
          {name: 'Japan 🇯🇵', value: 'JP', inline: true},
          {name: 'Mexico 🇲🇽', value: 'MX', inline: true},
          {name: 'Netherlands 🇳🇱', value: 'NL', inline: true},
          {name: 'Poland 🇵🇱', value: 'PL', inline: true},
          {name: 'Portugal 🇵🇹', value: 'PT', inline: true},
          {name: 'Russia 🇷🇺', value: 'RU', inline: true},
          {name: 'Saudi Arabia 🇸🇦', value: 'SA', inline: true},
          {name: 'Spain 🇪🇸', value: 'ES', inline: true},
          {name: 'Sweden 🇸🇪', value: 'SE', inline: true},
          {name: 'Switzerland 🇨🇭', value: 'CH', inline: true},
          {name: 'Ukraine 🇺🇦', value: 'UA', inline: true},
          {name: 'United States of America 🇺🇸', value: 'US', inline: true},
      );
}

/**
 * Return an embed who show a question with choices of answers.
 *
 * @param {question} question Question shown
 * @return {EmbedBuilder} Embed
 */
function returnEmbedQuestion(question) {
  const answers = [];
  answers.push(question.incorrectAnswers[0]);
  answers.push(question.incorrectAnswers[1]);
  answers.push(question.incorrectAnswers[2]);
  answers.push(question.correctAnswer);
  const answersRandom = answers.sort(function() {
    return Math.random() - 0.5;
  });
  return new EmbedBuilder()
      .setColor(Colors.DarkGold)
      .setTitle('Question ❔')
      .setDescription('Category : ' + question.category + '\n' +
        question.question + '. You have 30 seconds to answer.')
      .addFields(
          {name: 'Choice 1', value: answersRandom[0], inline: true},
          {name: 'Choice 2', value: answersRandom[1], inline: true},
          {name: 'Choice 3', value: answersRandom[2], inline: true},
          {name: 'Choice 4', value: answersRandom[3], inline: true},
      );
}

/**
 * Return an embed who show the end of answering time with a specific message.
 *
 * @param {string} message The message of the Embed
 * @return {EmbedBuilder} Embed
 */
function returnEmbedQuestionEnd(message) {
  return new EmbedBuilder()
      .setColor(Colors.Red)
      .setTitle('Answering time is over ⌛')
      .setDescription(message + ' Wait for the game leader to show the next' +
        ' question');
}

/**
 * Return the leaderboard's text.
 * @param {Array} players Players of the game
 * @param {Array} playersPoint Points of every player of the game
 * @return {string} The leaderboard's text
 */
function returnLeaderboardText(players, playersPoint) {
  let leaderboardText = `${players[0]} - ${playersPoint[0]} points\n`;
  if (players[1] !== undefined) {
    leaderboardText =
      `${leaderboardText} ${players[1]} - ${playersPoint[1]} points\n`;
  }
  if (players[2] !== undefined) {
    leaderboardText =
      `${leaderboardText} ${players[2]} - ${playersPoint[2]} points\n`;
  }
  if (players[3] !== undefined) {
    leaderboardText =
      `${leaderboardText} ${players[3]} - ${playersPoint[3]} points\n`;
  }
  return leaderboardText;
}

module.exports = {
  returnEmbebPlayers,
  returnEmbebHelp,
  returnEmbedGameEnded,
  returnEmbedShowRegion,
  returnEmbedHelpSet,
  returnEmbedQuestion,
  returnEmbedQuestionEnd,
  returnEmbedLeaderboard,
};
