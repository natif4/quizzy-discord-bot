const {SlashCommandBuilder} = require('discord.js');
const {returnEmbedShowRegion} = require('../embeds');
const {quiz} = require('../quiz');


module.exports = {
  data: new SlashCommandBuilder()
      .setName('reg')
      .setDescription('Show the actual region of the game and all other ' +
        'possibilies.'),
  async execute(interaction) {
    interaction.reply({embeds: [returnEmbedShowRegion(quiz.region)]});
  },
};
