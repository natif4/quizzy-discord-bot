const {REST, Routes} = require('discord.js');
const fs = require('node:fs');
const {clientId, guildId, token} = require('./config.json');

const commands = [];

const commandsFile =
  fs.readdirSync('./commands').filter((file) => file.endsWith('.js'));

for (const file of commandsFile) {
  const command = require(`./commands/${file}`);
  commands.push(command.data.toJSON());
}

const rest = new REST({version: '10'}).setToken(token);

(async () => {
  try {
    console.log('Started refreshing ' + commands.length +
      ' application (/) commands.');

    const data = await rest.put(
        Routes.applicationGuildCommands(clientId, guildId),
        {body: commands},
    );

    console.log('Successfully reloaded ' + data.length +
     ' application (/) commands.');
  } catch (erreur) {
    console.error(erreur);
  }
})();
