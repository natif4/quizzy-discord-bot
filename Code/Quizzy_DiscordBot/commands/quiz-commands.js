const {SlashCommandBuilder} = require('discord.js');
const {quiz} = require('../quiz.js');
const {returnEmbebPlayers, returnEmbedQuestion, returnEmbedGameEnded,
  returnEmbedQuestionEnd, returnEmbedLeaderboard} = require('../embeds');
const {getQuestions} = require('../api-question');

module.exports = {
  data: new SlashCommandBuilder()
      .setName('quiz')
      .setDescription('Quiz\'s commands')
      .addSubcommand((subcommand) =>
        subcommand
            .setName('new')
            .setDescription('Create a new game.'))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('question')
            .setDescription('Show a question.'))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('join')
            .setDescription('Join the game.'))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('start')
            .setDescription('Start the quiz game.'))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('info')
            .setDescription('Show info of the game.'))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('quit')
            .setDescription('Quit the quiz game.'))
      .addSubcommand((subcommand) =>
        subcommand
            .setName('leaderboard')
            .setDescription('Show the leaderboard of the game.')),
  async execute(interaction) {
    if (interaction.options.getSubcommand() === 'new') {
      createQuizGame(interaction);
    } else if (interaction.options.getSubcommand() === 'question') {
      showNextQuestion(interaction);
    } else if (interaction.options.getSubcommand() === 'join') {
      joinQuizGame(interaction);
    } else if (interaction.options.getSubcommand() === 'start') {
      startQuizGame(interaction);
    } else if (interaction.options.getSubcommand() === 'info') {
      showQuizGameInfo(interaction);
    } else if (interaction.options.getSubcommand() === 'quit') {
      quitQuizGame(interaction);
    } else if (interaction.options.getSubcommand() === 'leaderboard') {
      showLeaderBoard(interaction);
    }
  },
};

/**
 * Create a new quiz game.
 *
 * @param {object} interaction The interaction of the command.
 */
function createQuizGame(interaction) {
  if (!quiz.gameInitialized) {
    quiz.gameInitialized = true;
    quiz.playersName[quiz.playersNum] = interaction.user.tag;
    quiz.playersID[quiz.playersNum] = interaction.user.id;
    quiz.playersPoints[quiz.playersNum] = 0;
    quiz.gameLeader = interaction.user.tag;
    quiz.playersNum++;
    getQuestions();
    interaction.reply({embeds: [returnEmbebPlayers(quiz.playersName,
        quiz.difficulty, quiz.region, quiz.questionLimit.toString())],
    content: interaction.user.tag + ' has create a new quiz game.' +
        ' Wait for the game leader to start it. ⏲️'});
  } else {
    interaction.reply('There\'s already a quiz game initialized');
  }
}

/**
 * Show the next question of the quiz.
 *
 * @param {object} interaction The interaction of the command.
 */
function showNextQuestion(interaction) {
  if (quiz.gameInitialized) {
    if (quiz.gameStarted) {
      if (!quiz.questionMode) {
        if (interaction.user.tag === quiz.gameLeader) {
          showQuestion(interaction, quiz.questions[quiz.counter].correctAnswer);
        } else {
          interaction.reply('Only the game leader can execute this');
        }
      } else {
        interaction.reply(`There's already a question in progress !`);
      }
    } else {
      interaction.reply('The quiz game isn\'t even started yet.');
    }
  } else {
    showNoQuizCreated(interaction);
  }
}

/**
 * Show the quiz's leaderboard.
 *
 * @param {object} interaction The interaction of the command.
 */
function showLeaderBoard(interaction) {
  if (quiz.gameInitialized) {
    if (quiz.gameStarted) {
      if (quiz.playersID.includes(interaction.user.id)) {
        interaction.reply({embeds: [returnEmbedLeaderboard(quiz.playersName,
            quiz.playersPoints)]});
      }
    } else {
      interaction.reply('The quiz game isn\'t even started yet.');
    }
  } else {
    showNoQuizCreated(interaction);
  }
}

/**
 * Join the quiz game.
 *
 * @param {object} interaction The interaction of the command.
 */
function joinQuizGame(interaction) {
  if (quiz.gameInitialized && !quiz.gameStarted) {
    if (!quiz.playersName.includes(interaction.user.tag)) {
      if (quiz.playersNum < quiz.maxPlayers) {
        quiz.playersName[quiz.playersNum] = interaction.user.tag;
        quiz.playersID[quiz.playersNum] = interaction.user.id;
        quiz.playersPoints[quiz.playersNum] = 0;
        quiz.playersNum++;
        interaction.reply({embeds: [returnEmbebPlayers(quiz.playersName)],
          content: interaction.user.tag + ' has join the game. 👋'});
      } else {
        interaction.reply('The actual quiz game is full ! ⛔');
      }
    } else {
      interaction.reply('You already are in the game ! ⛔');
    }
  } else {
    showNoQuizCreated(interaction);
  }
}

/**
 * Start the quiz game.
 *
 * @param {object} interaction The interaction of the command.
 */
function startQuizGame(interaction) {
  if (quiz.gameInitialized) {
    if (!quiz.gameStarted) {
      if (interaction.user.tag === quiz.gameLeader) {
        quiz.gameStarted = true;
        showQuestion(interaction, quiz.questions[quiz.counter].correctAnswer);
      } else {
        interaction.reply('Only the game leader can execute this command. 🤓');
      }
    } else {
      interaction.reply('The game is already started');
    }
  } else {
    showNoQuizCreated(interaction);
  }
}
/**
 * Show info of the quiz game.
 *
 * @param {object} interaction The interaction of the command.
 */
function showQuizGameInfo(interaction) {
  if (quiz.gameInitialized) {
    interaction.reply({embeds: [returnEmbebPlayers(quiz.playersName)]});
  } else {
    showNoQuizCreated(interaction);
  }
}

/**
 * Quit the quiz game.
 *
 * @param {object} interaction The interaction of the command.
 */
function quitQuizGame(interaction) {
  if (quiz.gameInitialized) {
    if (quiz.gameLeader === interaction.user.tag) {
      const description = 'The game is over because the leader has quit. 😥' +
        ' Write "/quiz new" to begin a new game.';
      interaction.reply({embeds: [returnEmbedGameEnded(description)],
        content: interaction.user.tag + ' has left the game. 🙁'});
      initializeQuiz();
    } else if (quiz.playersName.includes(interaction.user.tag)) {
      quiz.playersNum--;
      quiz.playersName.splice(quiz.playersName
          .indexOf(interaction.user.tag), 1);
      quiz.playersID.splice(quiz.playersID.indexOf(interaction.user.id), 1);
      interaction.reply(interaction.user.tag + ' has left the game. 🙁');
    } else {
      interaction.reply('You\'re not even in the current game. 🙄');
    }
  } else {
    showNoQuizCreated(interaction);
  }
}

/**
 * Show a question and await the answer.
 *
 * @param {object} interaction The interaction of the command
 * @param {string} answer The good answer of the question
 */
function showQuestion(interaction, answer) {
  quiz.questionMode = true;
  const collectorFilter = (response) => {
    return response.content.toLowerCase() ===
      quiz.questions[quiz.counter].correctAnswer.toLowerCase() &&
      quiz.playersID.includes(response.author.id);
  };
  quiz.counter++;
  interaction.reply({embeds: [returnEmbedQuestion(
      quiz.questions[quiz.counter])]})
      .then(() => {
        interaction.channel.awaitMessages(
            {filter: collectorFilter, max: 1, time: 30000, errors: ['time']})
            .then((collected) => {
              interaction.followUp({embeds: [returnEmbedQuestionEnd(
                  collected.first().author.tag + ' got the correct answer!')]});
              endQuestion(collected.first().author.tag, interaction, true);
            })
            .catch(() => {
              interaction.followUp({embeds: [returnEmbedQuestionEnd(
                  'Looks like nobody got the answer this time.')]});
              endQuestion(undefined, interaction, false);
            });
      });
}

/**
 * Indicates that there no quiz game created so far.
 *
 * @param {object} interaction The interaction of the command
 */
function showNoQuizCreated(interaction) {
  interaction.reply('There are no quiz games created right now ! ⚠️' +
    'Execute the command "/quiz new" to create a new game 💁‍♂️');
}

/**
 * Do the necessary process after the end of a question.
 *
 * @param {string} playerTag The tag of the player that got the question
 * @param {object} interaction The interaction of the command
 * @param {boolean} succeed True if the question is succeeded, false if not.
 */
function endQuestion(playerTag, interaction, succeed) {
  quiz.questionMode = false;
  if (succeed) {
    const index = quiz.playersName.indexOf(playerTag);
    quiz.playersPoints[index] += 10;
  }
  if (quiz.counter === quiz.questionLimit) {
    interaction.followUp({embeds: [returnEmbedGameEnded(
        'There are no questions remaining! Write "/quiz new" to ' +
        'start a new game.')]});
    interaction.followUp({embeds: [returnEmbedLeaderboard(quiz.playersName,
        quiz.playersPoints)]});
    initializeQuiz();
  }
}

/**
 * Initialize the quiz to his default value.
 *
 */
function initializeQuiz() {
  quiz.gameInitialized = false;
  quiz.gameStarted = false;
  quiz.questionMode = false;
  quiz.playersNum = 0;
  quiz.playersName = [];
  quiz.playersID = [];
  quiz.gameLeader = '';
  quiz.questions = [];
  quiz.counter = 0;
}
